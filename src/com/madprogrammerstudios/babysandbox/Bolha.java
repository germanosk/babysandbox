/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.madprogrammerstudios.babysandbox;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.font.GlyphVector;
import java.util.Random;

/**
 *
 * @author germano
 */
public class Bolha {
    private int posX;
    private int posY;
    private boolean ativo = true;
    private Long inicio;
    private int tamanho;
    private Color outlineColor;
    private int alpha;
    public Bolha(int posX, int posY){
        this.posX = posX;
        this.posY = posY;
        inicio =System.nanoTime();
        tamanho = 80;
        alpha = 255;
        outlineColor = defineCor();
    }
    public boolean atualiza(){
        long tempoDecorrido = System.nanoTime()- inicio;
        if(tamanho < 120){
            if(tempoDecorrido > 500){
                tamanho++;
                inicio = System.nanoTime();
            }
        }
        else{
            if(tempoDecorrido > 150){
                if(alpha > 1){
                    outlineColor = new Color(outlineColor.getRed(),outlineColor.getGreen(),outlineColor.getBlue(),alpha);
                    alpha--;
                }
                else{
                    this.ativo = false;
                }
                inicio = System.nanoTime();
            }
        }
        return ativo;
        
    }
    public void desenha(Graphics2D g){
        Graphics2D g2;
        g2 = (Graphics2D)g.create();
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setRenderingHint(RenderingHints.KEY_RENDERING,RenderingHints.VALUE_RENDER_QUALITY);
        
        
         GradientPaint gp = new GradientPaint((float)posX, (float)posX, outlineColor,
                             posX+100, posX+100, Color.white);
        g2.setPaint(gp); 
      
      //  g2.setColor(outlineColor);
        g2.setStroke(new BasicStroke(4.0f));
       // g2.translate(posX,posY);
        g2.drawOval(posX, posY, tamanho, tamanho);  
    }
    public Color defineCor(){
        Color corAleatoria;
        String hex;
        Random aleatorio = new Random();
        switch(aleatorio.nextInt(10)){
            case 0:
                hex = "c5393c";
            break;
            case 1:
                hex = "5ABFC6";
            break;
            case 2:
                hex = "D1C57E";
            break;
            case 3:
                hex = "E85AAA";
            break;
            case 4:
                hex = "FF2626";
            break;
            case 5:
                hex = "009ACD";
            break;
            case 6:
                hex = "FF0000";
            break;
            case 7:
                hex = "9EFC7D";
            break;
            case 8:
                hex = "58A866";
            break;
            case 9:
                hex = "DD463F";
            break;
            default:
            hex = "c5393c";
            break;
        
        }
        
        int value = Integer.parseInt(hex, 16);
        corAleatoria = new Color(value);
        return corAleatoria;
    }
}
