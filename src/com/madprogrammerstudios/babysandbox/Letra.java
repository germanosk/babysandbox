/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.madprogrammerstudios.babysandbox;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.font.FontRenderContext;
import java.awt.font.GlyphVector;
import java.awt.font.TextLayout;
import java.awt.geom.AffineTransform;
import java.util.Date;
import java.util.Random;
import javax.swing.JFrame;

/**
 *
 * @author germano
 */
public class Letra {
    private int posX;
    private int posY;
    private String letra = "";
    private boolean ativo = true;
    private Long inicio;
    private int tamanho;
    private Color cor,outlineColor;
    private int alpha;
    
    public Letra(int posX, int posY, String letra){
        this.posX = posX;
        this.posY = posY;
        this.letra = letra;
        inicio =System.nanoTime();
        tamanho = 80;
        cor = defineCor();
        alpha = 255;
        outlineColor = Color.white;
    }
    public boolean atualiza(){
        long tempoDecorrido = System.nanoTime()- inicio;
        if(tamanho < 120){
            if(tempoDecorrido > 500){
                tamanho++;
                inicio = System.nanoTime();
            }
        }
        else{
            if(tempoDecorrido > 150){
                if(alpha > 1){
                    cor = new Color(cor.getRed(),cor.getGreen(),cor.getBlue(),alpha);
                    outlineColor = new Color(outlineColor.getRed(),outlineColor.getGreen(),outlineColor.getBlue(),alpha);
                    alpha--;
                }
                else{
                    this.ativo = false;
                }
                inicio = System.nanoTime();
            }
        }
        return ativo;
        
    }
    public void desenha(Graphics g){
        Graphics2D g2 = (Graphics2D) g;
        g2 = (Graphics2D)g2.create();
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setRenderingHint(RenderingHints.KEY_RENDERING,RenderingHints.VALUE_RENDER_QUALITY);
        g2.setColor(cor);
        Font fonte = new Font("Helvetica", Font.BOLD, tamanho);
        g2.setFont(fonte);
        g2.drawString(letra, posX, posY);
        
        FontMetrics fm1 = g2.getFontMetrics(fonte);
        GlyphVector gv = fonte.createGlyphVector(g2.getFontRenderContext(), letra);
        Shape shape = gv.getOutline();
        g2.setColor(outlineColor);
        g2.setStroke(new BasicStroke(4.0f));
        g2.translate(posX,posY);
        g2.draw(shape);  
    }
    
    public Color defineCor(){
        Color corAleatoria;
        String hex;
        Random aleatorio = new Random();
        switch(aleatorio.nextInt(10)){
            case 0:
                hex = "c5393c";
            break;
            case 1:
                hex = "5ABFC6";
            break;
            case 2:
                hex = "D1C57E";
            break;
            case 3:
                hex = "E85AAA";
            break;
            case 4:
                hex = "FF2626";
            break;
            case 5:
                hex = "009ACD";
            break;
            case 6:
                hex = "FF0000";
            break;
            case 7:
                hex = "9EFC7D";
            break;
            case 8:
                hex = "58A866";
            break;
            case 9:
                hex = "DD463F";
            break;
            default:
            hex = "c5393c";
            break;
        
        }
        
        int value = Integer.parseInt(hex, 16);
        corAleatoria = new Color(value);
        return corAleatoria;
    }

    public int getPosX() {
        return posX;
    }

    public void setPosX(int posX) {
        this.posX = posX;
    }

    public int getPosY() {
        return posY;
    }

    public void setPosY(int posY) {
        this.posY = posY;
    }

    public String getLetra() {
        return letra;
    }

    public void setLetra(String letra) {
        this.letra = letra;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public Long getInicio() {
        return inicio;
    }

    public void setInicio(Long inicio) {
        this.inicio = inicio;
    }

    public int getTamanho() {
        return tamanho;
    }

    public void setTamanho(int tamanho) {
        this.tamanho = tamanho;
    }

    public Color getCor() {
        return cor;
    }

    public void setCor(Color cor) {
        this.cor = cor;
    }

    public int getAlpha() {
        return alpha;
    }

    public void setAlpha(int alpha) {
        this.alpha = alpha;
    }
    
    
}
