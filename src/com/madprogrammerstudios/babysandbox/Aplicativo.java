/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.madprogrammerstudios.babysandbox;


import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import com.madprogrammerstudios.odonataj.GerenciadorEntrada;
import com.madprogrammerstudios.odonataj.Jogo;
import com.madprogrammerstudios.odonataj.Mouse;

/**
 *
 * @author GERMANO
 */
public class Aplicativo extends Jogo{
    int posX,posY;
    List<Letra> letras= new ArrayList<>();
    List<Bolha> bolhas = new ArrayList<>();
    Long temporizadorBolha;
    
    public Aplicativo() {
        super("TESTE DE BIBLIOTECA", 900, 760,true);
        KeyHook.blockWindowsKey();
//       this.janelaPrincipal.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        posX = 0;
        posY = 0;
        temporizadorBolha = System.currentTimeMillis();
    }

    @Override
    public void carregar() {
        
    }

    @Override
    public void descarregar() {
        
    }

    @Override
    public void atualizar() {
        
//                System.out.println("Posicao:"+Mouse.getInstance().getPosicao().x+","+Mouse.getInstance().getPosicao().y);
//                System.out.println("Bot�o:"+Mouse.getInstance().getBotao());
//                System.out.println("Posicao:"+Mouse.getInstance().getPosicao());
                if(GerenciadorEntrada.getInstance().isPressed(KeyEvent.VK_F12) && GerenciadorEntrada.getInstance().isPressed(KeyEvent.VK_F1)){
                
                    this.terminar();
                    
                    this.unload();
                    this.descarregar();
                     KeyHook.unblockWindowsKey();
                     System.exit(0);
                }
                if(GerenciadorEntrada.getInstance().isPressed(KeyEvent.VK_A) && !hasLetra("A")){
                   letras.add(new Letra(posicaoAleatoria("x"), posicaoAleatoria("y"), "A"));
                }
                if(GerenciadorEntrada.getInstance().isPressed(KeyEvent.VK_B) && !hasLetra("B")){
                    letras.add(new Letra(posicaoAleatoria("x"), posicaoAleatoria("y"), "B"));
                }
                if(GerenciadorEntrada.getInstance().isPressed(KeyEvent.VK_C) && !hasLetra("C")){
                    letras.add(new Letra(posicaoAleatoria("x"), posicaoAleatoria("y"), "C"));
                }
                if(GerenciadorEntrada.getInstance().isPressed(KeyEvent.VK_D) && !hasLetra("D")){
                    letras.add(new Letra(posicaoAleatoria("x"), posicaoAleatoria("y"), "D"));
                }
                if(GerenciadorEntrada.getInstance().isPressed(KeyEvent.VK_E) && !hasLetra("E")){
                    letras.add(new Letra(posicaoAleatoria("x"), posicaoAleatoria("y"), "E"));
                }
                if(GerenciadorEntrada.getInstance().isPressed(KeyEvent.VK_F) && !hasLetra("F")){
                    letras.add(new Letra(posicaoAleatoria("x"), posicaoAleatoria("y"), "F"));
                }
                if(GerenciadorEntrada.getInstance().isPressed(KeyEvent.VK_G) && !hasLetra("G")){
                    letras.add(new Letra(posicaoAleatoria("x"), posicaoAleatoria("y"), "G"));
                }
                if(GerenciadorEntrada.getInstance().isPressed(KeyEvent.VK_H) && !hasLetra("H")){
                    letras.add(new Letra(posicaoAleatoria("x"), posicaoAleatoria("y"), "H"));
                }
                if(GerenciadorEntrada.getInstance().isPressed(KeyEvent.VK_I) && !hasLetra("I")){
                    letras.add(new Letra(posicaoAleatoria("x"), posicaoAleatoria("y"), "I"));
                }
                if(GerenciadorEntrada.getInstance().isPressed(KeyEvent.VK_J) && !hasLetra("J")){
                    letras.add(new Letra(posicaoAleatoria("x"), posicaoAleatoria("y"), "J"));
                }
                if(GerenciadorEntrada.getInstance().isPressed(KeyEvent.VK_K) && !hasLetra("K")){
                    letras.add(new Letra(posicaoAleatoria("x"), posicaoAleatoria("y"), "K"));
                }
                if(GerenciadorEntrada.getInstance().isPressed(KeyEvent.VK_L) && !hasLetra("L")){
                    letras.add(new Letra(posicaoAleatoria("x"), posicaoAleatoria("y"), "L"));
                }
                if(GerenciadorEntrada.getInstance().isPressed(KeyEvent.VK_M) && !hasLetra("M")){
                    letras.add(new Letra(posicaoAleatoria("x"), posicaoAleatoria("y"), "M"));
                }
                if(GerenciadorEntrada.getInstance().isPressed(KeyEvent.VK_N) && !hasLetra("N")){
                    letras.add(new Letra(posicaoAleatoria("x"), posicaoAleatoria("y"), "N"));
                }
                if(GerenciadorEntrada.getInstance().isPressed(KeyEvent.VK_O) && !hasLetra("O")){
                    letras.add(new Letra(posicaoAleatoria("x"), posicaoAleatoria("y"), "O"));
                }
                if(GerenciadorEntrada.getInstance().isPressed(KeyEvent.VK_P) && !hasLetra("P")){
                    letras.add(new Letra(posicaoAleatoria("x"), posicaoAleatoria("y"), "P"));
                }
                if(GerenciadorEntrada.getInstance().isPressed(KeyEvent.VK_Q) && !hasLetra("Q")){
                    letras.add(new Letra(posicaoAleatoria("x"), posicaoAleatoria("y"), "Q"));
                }
                if(GerenciadorEntrada.getInstance().isPressed(KeyEvent.VK_R) && !hasLetra("R")){
                    letras.add(new Letra(posicaoAleatoria("x"), posicaoAleatoria("y"), "R"));
                }
                if(GerenciadorEntrada.getInstance().isPressed(KeyEvent.VK_R) && !hasLetra("R")){
                    letras.add(new Letra(posicaoAleatoria("x"), posicaoAleatoria("y"), "R"));
                }
                if(GerenciadorEntrada.getInstance().isPressed(KeyEvent.VK_S) && !hasLetra("S")){
                    letras.add(new Letra(posicaoAleatoria("x"), posicaoAleatoria("y"), "S"));
                }
                if(GerenciadorEntrada.getInstance().isPressed(KeyEvent.VK_T) && !hasLetra("T")){
                    letras.add(new Letra(posicaoAleatoria("x"), posicaoAleatoria("y"), "T"));
                }
                if(GerenciadorEntrada.getInstance().isPressed(KeyEvent.VK_U) && !hasLetra("U")){
                    letras.add(new Letra(posicaoAleatoria("x"), posicaoAleatoria("y"), "U"));
                }
                if(GerenciadorEntrada.getInstance().isPressed(KeyEvent.VK_V) && !hasLetra("V")){
                    letras.add(new Letra(posicaoAleatoria("x"), posicaoAleatoria("y"), "V"));
                }
                if(GerenciadorEntrada.getInstance().isPressed(KeyEvent.VK_X) && !hasLetra("X")){
                    letras.add(new Letra(posicaoAleatoria("x"), posicaoAleatoria("y"), "X"));
                }
                if(GerenciadorEntrada.getInstance().isPressed(KeyEvent.VK_Y) && !hasLetra("Y")){
                    letras.add(new Letra(posicaoAleatoria("x"), posicaoAleatoria("y"), "Y"));
                }
                if(GerenciadorEntrada.getInstance().isPressed(KeyEvent.VK_W) && !hasLetra("W")){
                    letras.add(new Letra(posicaoAleatoria("x"), posicaoAleatoria("y"), "W"));
                }
                if(GerenciadorEntrada.getInstance().isPressed(KeyEvent.VK_Z) && !hasLetra("Z")){
                    letras.add(new Letra(posicaoAleatoria("x"), posicaoAleatoria("y"), "Z"));
                }
                if((GerenciadorEntrada.getInstance().isPressed(KeyEvent.VK_NUMPAD1)||GerenciadorEntrada.getInstance().isPressed(KeyEvent.VK_1)) && !hasLetra("1")){
                    letras.add(new Letra(posicaoAleatoria("x"), posicaoAleatoria("y"), "1"));
                }
                if((GerenciadorEntrada.getInstance().isPressed(KeyEvent.VK_NUMPAD2)||GerenciadorEntrada.getInstance().isPressed(KeyEvent.VK_2)) && !hasLetra("2")){
                    letras.add(new Letra(posicaoAleatoria("x"), posicaoAleatoria("y"), "2"));
                }
                if((GerenciadorEntrada.getInstance().isPressed(KeyEvent.VK_NUMPAD3)||GerenciadorEntrada.getInstance().isPressed(KeyEvent.VK_3)) && !hasLetra("3")){
                    letras.add(new Letra(posicaoAleatoria("x"), posicaoAleatoria("y"), "3"));
                }
                if((GerenciadorEntrada.getInstance().isPressed(KeyEvent.VK_NUMPAD4)||GerenciadorEntrada.getInstance().isPressed(KeyEvent.VK_4)) && !hasLetra("4")){
                    letras.add(new Letra(posicaoAleatoria("x"), posicaoAleatoria("y"), "4"));
                }
                if((GerenciadorEntrada.getInstance().isPressed(KeyEvent.VK_NUMPAD5)||GerenciadorEntrada.getInstance().isPressed(KeyEvent.VK_5)) && !hasLetra("5")){
                    letras.add(new Letra(posicaoAleatoria("x"), posicaoAleatoria("y"), "5"));
                }
                if((GerenciadorEntrada.getInstance().isPressed(KeyEvent.VK_NUMPAD6)||GerenciadorEntrada.getInstance().isPressed(KeyEvent.VK_6)) && !hasLetra("6")){
                    letras.add(new Letra(posicaoAleatoria("x"), posicaoAleatoria("y"), "6"));
                }
                if((GerenciadorEntrada.getInstance().isPressed(KeyEvent.VK_NUMPAD7)||GerenciadorEntrada.getInstance().isPressed(KeyEvent.VK_7)) && !hasLetra("7")){
                    letras.add(new Letra(posicaoAleatoria("x"), posicaoAleatoria("y"), "7"));
                }
                if((GerenciadorEntrada.getInstance().isPressed(KeyEvent.VK_NUMPAD8)||GerenciadorEntrada.getInstance().isPressed(KeyEvent.VK_8)) && !hasLetra("8")){
                    letras.add(new Letra(posicaoAleatoria("x"), posicaoAleatoria("y"), "8"));
                }
                if((GerenciadorEntrada.getInstance().isPressed(KeyEvent.VK_NUMPAD9)||GerenciadorEntrada.getInstance().isPressed(KeyEvent.VK_9)) && !hasLetra("9")){
                    letras.add(new Letra(posicaoAleatoria("x"), posicaoAleatoria("y"), "9"));
                }
                if((GerenciadorEntrada.getInstance().isPressed(KeyEvent.VK_NUMPAD0)||GerenciadorEntrada.getInstance().isPressed(KeyEvent.VK_0)) && !hasLetra("0")){
                     letras.add(new Letra(posicaoAleatoria("x"), posicaoAleatoria("y"), "0"));
                }
                if(GerenciadorEntrada.getInstance().isPressed(KeyEvent.VK_DEAD_CEDILLA) && !hasLetra("Ç")){
                    letras.add(new Letra(posicaoAleatoria("x"), posicaoAleatoria("y"), "Ç"));
                } 
                List<Letra> listaRemocao = new ArrayList<>();
                for(Letra l: letras){
                    if(!l.atualiza()){
                        listaRemocao.add(l);
                    }
                }
                letras.removeAll(listaRemocao);
                updateBolhas();
    }

    @Override
    public void desenhar(Graphics2D g) {
        g.setColor(new Color(104, 183, 151));
        g.fillRect(0, 0, janelaPrincipal.getWidth(), janelaPrincipal.getHeight());
        desenhaBolhas(g);
        g.setFont(new Font("", Font.BOLD, 50));
        String hex = "7fc5393c";
        int value = Integer.parseInt(hex, 16);
        g.setColor(new Color(value, true));
        g.drawString("Para sair pressione F1 + F12", 10, 50);
//        g.drawString("Posicao:"+Mouse.getInstance().getPosicao(), 10, 50);
//        g.drawString("Botão:"+Mouse.getInstance().getBotao(), 150, 50);
        
        for(Letra l: letras){
             l.desenha(g);
        }
    }
    
    public boolean hasLetra(String letra){
        boolean has = false;
        if(this.letras.size() > 0 ){
            for(Letra l: letras){
                if(l.getLetra().equals(letra)){
                    has = true;
                }
            }
        }
        return has;
    }
     public Integer posicaoAleatoria(String s){
         Random aleatorio = new Random();
         if(s.equals("x")){
         return aleatorio.nextInt(this.getWidth());
         }
         else{
         return aleatorio.nextInt(this.getHeight());
         }
     }
     public void updateBolhas(){
         List<Bolha> listaRemocao = new ArrayList<>();
         if(bolhas.size()>0){
            for(Bolha b: bolhas){
                if(!b.atualiza()){
                listaRemocao.add(b);
                }
            }
         }
         bolhas.removeAll(listaRemocao);
         int newX = Mouse.getInstance().getPosicao().x;
         int newY = Mouse.getInstance().getPosicao().y;
         long tempoDecorrido = System.currentTimeMillis()- temporizadorBolha;
         if((posX != newX || posY != newY) && tempoDecorrido> 200){
             posX = newX;
             posY = newY;
             bolhas.add(new Bolha(posX, posY));
             temporizadorBolha = System.currentTimeMillis();
         }
         
     }
     private void desenhaBolhas(Graphics2D g){
         if(bolhas.size()>0){
            for(Bolha b: bolhas){
                b.desenha(g);
            }
         }
     }
}
