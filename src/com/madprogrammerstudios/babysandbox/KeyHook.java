/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.madprogrammerstudios.babysandbox;

/**
 *
 * @author germano
 */

    //import jna.platform.win32.Kernel32;
import com.sun.jna.platform.win32.Kernel32;
    import com.sun.jna.platform.win32.User32;
    import com.sun.jna.platform.win32.WinDef.HMODULE;
    import com.sun.jna.platform.win32.WinDef.LRESULT;
    import com.sun.jna.platform.win32.WinDef.WPARAM;
    import com.sun.jna.platform.win32.WinUser.HHOOK;
    import com.sun.jna.platform.win32.WinUser.KBDLLHOOKSTRUCT;
    import com.sun.jna.platform.win32.WinUser.LowLevelKeyboardProc;
    import com.sun.jna.platform.win32.WinUser.MSG;


    public class KeyHook {
        private static HHOOK hhk;
        private static LowLevelKeyboardProc keyboardHook;
        private static User32 lib;

        public static void blockWindowsKey() {
            if (isWindows()) {
                new Thread(new Runnable() {

                    @Override
                    public void run() {
                        lib = User32.INSTANCE;
                        HMODULE hMod = Kernel32.INSTANCE.GetModuleHandle(null);
                        keyboardHook = new LowLevelKeyboardProc() {
                            public LRESULT callback(int nCode, WPARAM wParam, KBDLLHOOKSTRUCT info) {
                                if (nCode >= 0) {
                                    switch (info.vkCode){
                                        case 0x5B:
                                        case 0x20: //Space
                                        case 0x5C:
                                        case 0x12:
                                        case 0x09: 
                                        case 0x1B: 
                                        case 0x0D:
                                        case 0x73:
                                        case 0xAC:
                                        case 0xB4:
                                        case 0x5D:
                                        case 0x5E: //Power
                                        case 0x5F: //Sleep
                                        case 0x37: //Power
                                        case 0x3F: //Sleep
                                        case 0x81:
                                        
                                        case 0x2E://delete
                                        case 0xA2://lft ctrl
                                        case 0xA3://rgt ctrl
                                        case     0x7C: //F13
                                        case     0x7D:
                                        case     0x7E:
                                        case     0x7F:
                                        case     0x80:
                                    //    case     0x81:// F18 & 
                                        case     0x82:
                                        case     0x83:
                                        case     0x84:
                                        case     0x85:
                                        case     0x86:
                                        case     0x87: //f24
                                        case     0xB6: //My Computer
                                     //   case     182:           
                                                    
                                                    
                                        return new LRESULT(1);
                                        default: //do nothing
                                            System.out.println(info.vkCode);
                                    }
                                }
                                return lib.CallNextHookEx(hhk, nCode, wParam, info.getPointer());
                            }
                        };
                        hhk = lib.SetWindowsHookEx(13, keyboardHook, hMod, 0);

                        // This bit never returns from GetMessage
                        int result;
                        MSG msg = new MSG();
                        while ((result = lib.GetMessage(msg, null, 0, 0)) != 0) {
                            if (result == -1) {
                                break;
                            } else {
                                lib.TranslateMessage(msg);
                                lib.DispatchMessage(msg);
                            }
                        }
                        lib.UnhookWindowsHookEx(hhk);
                    }
                }).start();
            }
        }

        public static void unblockWindowsKey() {
            if (isWindows() && lib != null) {
                lib.UnhookWindowsHookEx(hhk);
            }
        }

        public static boolean isWindows(){
            String os = System.getProperty("os.name").toLowerCase();
//            System.err.println(System.getProperty("os.name").toLowerCase()+(os.indexOf( "win" ) >= 0));
            return (os.indexOf( "win" ) >= 0);
        }
    }